@extends('theme.layout.master_layout')    
        
@section('pagecss')


@endsection


@section('pagecontent')

            <div class="content">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Login</h4>
                            </div>
                            <div class="content center-block">

                                @include('theme.layout.errors')

                                <form name="login" method="post" action="{{url('/login')}}">        


                                    {{csrf_field()}}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" name="txtemail" id="txtemail" class="form-control" placeholder="abc@gmail.com" value="">
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="Password" name="txtpassword" id="txtpassword" class="form-control" value="">
                                            </div>
                                        </div>
                                       
                                    </div>

                                    
                                        <button type="submit" class="btn btn-info btn-fill center-block">Login</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>

@endsection

@section('pagejavascript')



@endsection