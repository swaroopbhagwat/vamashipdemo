@extends('theme.layout.master_layout')    
        
@section('pagecss')


@endsection


@section('pagecontent')


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Address Book</h4>
                                <p class="category">Here is a List of your addresses</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>SR.No</th>
                                    	<th>Contact Name</th>
                                    	<th>Contact No.</th>
                                    	<th>Address Title</th>
                                    	<th>Country</th>
                                        <th>State</th>
                                        <th>Pincode</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>

                                        @if($addresses)
                                        @for($i=0;$i<count($addresses);$i++)
                                        <tr>

                                        	<td>{{$i+1}}</td>
                                        	<td>{{$addresses[$i]['contact_person_name']}}</td>
                                        	<td>{{$addresses[$i]['contact_person_number']}}</td>
                                        	<td>{{$addresses[$i]['address_book_title']}}</td>
                                        	<td>{{$addresses[$i]['country_name']}}</td>
                                            <td>{{$addresses[$i]['state_name']}}</td>
                                            <td>{{$addresses[$i]['pincode']}}</td>

                                            <td>
                                                <a href="{{url('/address-edit/'.$addresses[$i]['address_id'])}}">Edit</a>&nbsp;&nbsp;&nbsp;
                                                <a class="confirmation" href="{{url('/address-delete/'.$addresses[$i]['address_id'])}}">Delete</a>

                                                

                                            </td>
                                        </tr>

                                        @endfor
                                        @endif
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>

@endsection

@section('pagejavascript')

<script type="text/javascript">
    /*
                                                        //$('.example2').on('click', function () {
                                                            $.confirm1({
                                                                title: 'Confirm!',
                                                                content: 'Simple confirm!',
                                                                buttons: {
                                                                    confirm: function () {
                                                                        $.alert('Confirmed!');
                                                                    },
                                                                    cancel: function () {
                                                                        $.alert('Canceled!');
                                                                    },
                                                                    somethingElse: {
                                                                        text: 'Something else',
                                                                        btnClass: 'btn-blue',
                                                                        keys: [
                                                                            'enter',
                                                                            'shift'
                                                                        ],
                                                                        action: function () {
                                                                            this.$content // reference to the content
                                                                            $.alert('Something else?');
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                       // });
    */                                                   
</script>

<script type="text/javascript">
    $('.confirmation').on('click', function () {
        return confirm('Are You Sure ,You Want To Delete This Address?');
    });
</script>


@endsection