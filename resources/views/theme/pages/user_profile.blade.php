@extends('theme.layout.master_layout')    
        
@section('pagecss')


@endsection


@section('pagecontent')

            <div class="content">
            <div class="container-fluid ">
                <div class="row">


                    <div class="col-md-4">
                            <div class="card card-user">

                                <div class="card-image">
                                    <img  src="{{Get_Images_Url()}}background.jpg" width="330"  alt="...">
                                </div>
                                
                                <div class="card-body">
                                    <div class="author">
                                            
                                            <img class="avatar border-gray" src="{{Get_Images_Url()}}faces/face-3.jpg" alt="...">
                                            <h5 class="title">Name:{{$user_details->name}}</h5>
                                        <p class="description">
                                            Email:{{$user_details->email}}
                                        </p>
                                    </div>
                                    <p class="description text-center">
                                        Address Book Management
                                        <br> System.
                                        
                                    </p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    

@endsection

@section('pagejavascript')



@endsection