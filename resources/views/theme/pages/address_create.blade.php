@extends('theme.layout.master_layout')    
        
@section('pagecss')


@endsection


@section('pagecontent')

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Address</h4>
                            </div>
                            <div class="content">

                                @include('theme.layout.errors')

                                @if($default_array['address_id'] > '')

                                    <form name="address-update" method="post" action="{{url('/address-update')}}">
                                    <input type="hidden" name="txtaddress_id" value="{{$default_array['address_id']}}">

                                    @else
                                    <form name="address-create" method="post" action="{{url('/address-store')}}">        


                                @endif    

                                

                                    {{csrf_field()}}

                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Address Title</label>
                                                <input type="text" name="txtaddress_book_title" id="txtaddress_book_title" class="form-control" placeholder="Home,Work etc" value="{{$default_array['address_book_title']}}">
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Contact Person</label>
                                                <input type="text" class="form-control" name="txtcontact_person_name" placeholder="Name" value="{{$default_array['contact_person_name']}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txtcontact_person_number">Contact Number</label>
                                                <input type="text" name="txtcontact_person_number" class="form-control" placeholder="Number" value="{{$default_array['contact_person_number']}}">
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Address Line 1</label>
                                                <input type="text" name="txtaddressline1" class="form-control" placeholder="Building Name" value="{{$default_array['addressline1']}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Address Line 2</label>
                                                <input type="text" name="txtaddressline2" class="form-control" placeholder="Area,Road" value="{{$default_array['addressline2']}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Address Line 3</label>
                                                <input type="text" name="txtaddressline3" class="form-control" placeholder="Landmark" value="{{$default_array['addressline3']}}">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="text" name="txtpincode" class="form-control" placeholder="ZIP Code" value="{{$default_array['pincode']}}">
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row">
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City</label>
                                                <div class="col-md-12">
                                                  <select class="select2_single form-control" id="txtcityid" name="txtcityid" tabindex="-1">
                                                    <option value="">Select City</option>
                                                    
                                                    {!! $city_combo !!}

                                                  </select>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>State</label>

                                                <div class="col-md-12">
                                                  <select class="select2_single form-control" id="txtstateid" name="txtstateid" tabindex="-1">
                                                    <option value="">Select State</option>
                                                    
                                                    {!! $state_combo !!}
                                                    
                                                   

                                                  </select>
                                                </div>

                                                
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country</label>
                                                <div class="col-md-12">
                                                  <select class="select2_single form-control" id="txtcountryid" name="txtcountryid" tabindex="-1">
                                                    <option value="">Select Country</option>
                                                    
                                                    {!! $country_combo !!}

                                                  </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                        <button type="submit" class="btn btn-info btn-fill center-block">Submit</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>

@endsection

@section('pagejavascript')



@endsection