<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{APP_NAME}}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{Get_Public_Css_Url()}}bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{Get_Public_Css_Url()}}animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{Get_Public_Css_Url()}}light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <!-- <link href="http://localhost/vamaship/resources/views/theme/assets/css/demo.css" rel="stylesheet" /> -->


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{Get_Public_Css_Url()}}pe-icon-7-stroke.css" rel="stylesheet" />


    @yield('pagecss')

</head>
<body>

<div class="wrapper">

        
    <div class="sidebar" data-color="purple" data-image="{{Get_Images_Url()}}sidebar-5.jpg" >

        <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

        -->

        @include('theme.layout.left_nav_layout')



    </div>



    <div class="main-panel">
        <!--here will be page content-->

        @include('theme.layout.top_nav_layout')


        @yield('pagecontent')


        @include('theme.layout.footer_layout')

    </div>    






</div>


</body>

    <!--   Core JS Files   -->
    <script src="{{Get_Public_Js_Url()}}jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="{{Get_Public_Js_Url()}}bootstrap.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<!-- <script src="http://localhost/vamaship/resources/views/theme/assets/js/chartist.min.js"></script> -->

    
    <!--  Notifications Plugin    -->
    <!-- <script src="http://localhost/vamaship/resources/views/theme/assets/js/bootstrap-notify.js"></script> -->

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{Get_Public_Js_Url()}}light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<!-- <script src="http://localhost/vamaship/resources/views/theme/assets/js/demo.js"></script> -->


        @yield('pagejavascript')


	

</html>
