   
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{url('user-home')}}" class="simple-text">
                    {{APP_NAME}}
                </a>
            </div>

            <ul class="nav">
                 @if(Auth::check())
                <li>
                    <a href="{{url('/address-index')}}">
                        <i class="pe-7s-note2"></i>
                        <p>Address Book</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('/address-create')}}">
                        <i class="pe-7s-news-paper"></i>
                        <p>Add New Address</p>
                    </a>
                </li>
                <li>

                           <a href="{{url('/user-profile')}}">
                            <i class="pe-7s-user"></i>
                               <p>{{Auth::user()->name}}</p>
                            </a>

                </li>
                <li>
                     <a href="{{url('/user-logout')}}">
                        <i class="pe-7s-back"></i>
                                <p>Log out</p>
                    </a>   
                </li>
                
                @endif
                
            </ul>
    	</div>
