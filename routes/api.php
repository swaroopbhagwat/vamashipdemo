<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['apiProjectAuth']], function() {

	Route::get('/ip',function(){
		return "hello";
	});

	Route::post('/login','Auth\SessionController@api_login');

	Route::group(['middleware' => ['apiTokenAuth']], function() {

		

		Route::get('/address-show/{addressid}','AddressbookController@api_address_show');

		Route::get('/address-list','AddressbookController@api_address_list');

		Route::post('/address-create','AddressbookController@api_address_create');

		Route::put('/address-update','AddressbookController@api_address_update');

		Route::delete('/address-delete/{aid}','AddressbookController@api_address_delete');

	});

});