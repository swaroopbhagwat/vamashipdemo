<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','Auth\SessionController@create_login');

Route::post('/login','Auth\SessionController@userlogin');

Route::get('/help',function(){

	return view('theme.pages.help');
});


Route::group(['middleware' => ['webUserAuth']], function() {

		Route::get('/user-logout', 'Auth\SessionController@userlogout');

		Route::get('/user-home','AddressbookController@index');
			
		Route::get('/user-profile','UserController@user_profile');

		Route::get('/address-create','AddressbookController@create');

		Route::get('/address-edit/{aid}','AddressbookController@edit');

		Route::post('/address-update','AddressbookController@update');

		Route::get('/address-delete/{aid}','AddressbookController@delete');

		Route::post('/address-store','AddressbookController@store');

		Route::get('/address-index','AddressbookController@index');

});


Route::get('/page',function(){

return view('theme.pages.page');	

});