<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\User;

use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


    	
    	
        User::truncate();

        
          User::create( [
            'slug'=>'vamaship-user',
            'name'=>'vamaship user',
            'email'=>'demo@vamaship.com',
            'password'=>Hash::make('vamaship'),
            'api_token'=>'vamaship-user-token',
            
            ] );

          

    }
}
