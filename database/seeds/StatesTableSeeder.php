<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


    	$faker = Faker::create();

    	
        State::truncate();

        
          State::create( [
            'countryid'=>'1',
            'state_name'=>'Maharashtra',
            
            ] );

           State::create( [
            'countryid'=>'1',
            'state_name'=>'Karnataka',
            
            ] );

           State::create( [
            'countryid'=>'2',
            'state_name'=>'New South Wales',
            
            ] );

            State::create( [
            'countryid'=>'2',
            'state_name'=>'Western Australia',
            
            ] );

            State::create( [
            'countryid'=>'3',
            'state_name'=>'New Hampshire',
            
            ] );

            State::create( [
            'countryid'=>'3',
            'state_name'=>'Vermont',
            
            ] );


    }
}
