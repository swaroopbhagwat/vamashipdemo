<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\Country;

use App\State;

use App\City;


class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


    	$faker = Faker::create();

    	$countries = collect(Country::pluck('id'))->toArray();


    	$states = collect(State::pluck('id'))->toArray();
    	//dd($countries);

        City::truncate();

        foreach (range(1,15) as $index) 
        {
        	City::create([
            	'countryid'    => $countries[array_rand($countries,1)],
            	'stateid' 	   => $states[array_rand($states,1)],
            	'city_name'    => $faker->city,	
            	
        		]);

        }



    }
}
