<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addressbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('userid');
            $table->index('userid','idx_userid');
            $table->string('address_book_title');//Home,Work
            $table->string('contact_person_name');
            $table->string('contact_person_number');
            $table->string('addressline1');
            $table->string('addressline2');
            $table->string('addressline3')->nullable();
            $table->string('pincode');
            $table->unsignedInteger('countryid');
            $table->index('countryid','idx_countryid');
            $table->unsignedInteger('stateid');
            $table->index('stateid','idx_stateid');
            $table->unsignedInteger('cityid');
            $table->index('cityid','idx_cityid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addressbooks');
    }
}
