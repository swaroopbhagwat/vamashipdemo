<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->Increments('id');
            $table->UnsignedInteger('countryid');
            $table->index('countryid','idx_countryid');
            $table->UnsignedInteger('stateid');
            $table->index('stateid','idx_stateid');
            $table->string('city_name');
            $table->unique('city_name', 'unique_city_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
