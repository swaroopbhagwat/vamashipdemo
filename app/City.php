<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //

	public static function city($cityid)
    {
    	$city_obj = City::select('id','city_name')->where('id','=',$cityid)->first();
    
        return $city_obj;

    }


    public static function city_list($stateid='')
    {

    	if($stateid > '')
        {
            $allcities = City::select('id','city_name')
                        ->where('stateid','=',$stateid)
                        ->orderby('id','asc')
                        ->get();



        }    
        else
        {
            $allcities = City::select('id','city_name')
                        ->orderby('id','asc')     
                        ->get();


        }

                       

        return $allcities;

    }




}
