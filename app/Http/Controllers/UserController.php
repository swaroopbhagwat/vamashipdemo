<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    //

	public function user_profile()
	{
		$user_details = User::select('*')->where('id','=',auth()->user()->id)->first();

		//return $user_details;

		return view('theme.pages.user_profile',compact('user_details'));


	}


}
