<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\Http\Controllers\ApiErrorController;

use App\Http\Resources\Api_errorResourse;

use Auth;

use App\User;

use Validator;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;

use App\Http\Resources\UserResource;



class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userlogin(Request $request)
    {
        //

        //dd(request()->all());

        
        //validate  form
        $this->validate(request(),[
            'txtemail' => 'required|email',
            'txtpassword' => 'required'
            
        ]);

        //attempt to authenticate user

        $email = request('txtemail');
        $password = request('txtpassword');
        

            if (auth()->attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            
            return redirect('/user-home');    
            }
            else
            {

                return redirect()->back()->withErrors(['message' => 'Unauthorised User.']);
            }    

        
       
    }

    function api_login()
    {

        
        $error_check_obj = new ApiErrorController();
            
        $rules = array(
            'email' => 'required|email',
            'password' => 'required',
            
        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
            'email' => 'Email',
            'password' => 'Password',
            );

            $validator = Validator::make(request()->all(),$rules);
            $validator->setAttributeNames($friendly_names);   

        if ($validator->fails()) 
        {    

            return $error_check_obj->respondValidationError($validator->messages());
        }
        else 
        {
            $email = trim(request('email'));
            $password = trim(request('password'));
            

            if (auth()->attempt(['email' => $email, 'password' => $password])) 
            {

                $user = User::getUserObject($email); // this function you will find in user
            
                //return $user;
                $responses = $error_check_obj->respondWithOk(new UserResource($user));

            }
            else
            {
                $responses = $error_check_obj->respondUnauthorized();

            }    

        }

           return $responses; 
          




    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userlogout()
    {
        //
        auth()->logout();

        return redirect('/login');

    }

   

    public function create_login()
    {
        //
        $session_flag = Auth::check();
            if($session_flag == false)
            {    

                return view('theme.pages.login');

            }
            else
            {
                if(auth()->user())
                {
                    return redirect('/user-home');
                }
                else
                {
                    return redirect('/user-logout');

                }    
            }
        
        

    }

    
}
