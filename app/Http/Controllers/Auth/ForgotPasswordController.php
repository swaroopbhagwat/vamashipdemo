<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use App\Http\Controllers\OutgoingMailsController;

use Validator;

use App\User;

use App\Password_reset;

use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\ApiErrorController;

use App\Rules\ValidUseremail;

use App\Rules\CheckValidPassword;

use App\Http\Resources\UserResource;

use App\Http\Resources\EmployerResource;

use Illuminate\Support\Facades\DB;

use App\Jobs\SendEmail;


//use Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function display_forgot_form()
    {

       return view('theme1.web.forgot_password');     
    }

    public function create_jobseeker_reset_request()
    {

        $rules = array(
                        'email'                => 'required|email',    
                        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
                        'email'                => 'Email',    
                        );
        
        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            //return response()->json($validator->messages(), 500);//use this to insert status code
            
            $validator->validate();
            //if its api request then below response
            //return response()->json($validator->messages(),422);
            //if its 
        }
        else
        {
            //request()->all();
            $user = User::getUserObject(request('email'),JOBSEEKERCATID);
            $reset_obj = new Password_reset();

            $reset_obj->userid      =  $user->id;
            $reset_obj->email       =  request('email');
            $key_to_hash            = request('email').time();
            $generated_token        = base64_encode($key_to_hash);
            $reset_obj->token       = $generated_token; 
            $dt = Carbon::today();
            $validity_date = $dt->addHours(RESET_PASSWORD_LINK_VALIDITY_HOURS);
            $reset_obj->valid_till  =  $validity_date;

            $save_object = $reset_obj->save();            

            if($save_object == true)
            {

                $mail_obj = new OutgoingMailsController();
                $mail_obj->send_password_reset_link_mail($reset_obj,$generated_token);

                $title   = "Forgot Password";
                $message = "Password Reset Link Sent To Your Email Id.";

                return view("theme1.notifypages.success",compact(['title','message']));

            }    
            

        }    
       
       
    }


    public function create_jobseeker_reset_request_by_consultant($email)
    {

        
            //request()->all();
            $user = User::getUserObject($email,JOBSEEKERCATID);
            $reset_obj = new Password_reset();

            $reset_obj->userid      =  $user->id;
            $reset_obj->email       =  $email;
            $key_to_hash            = $email.time();
            $generated_token        = base64_encode($key_to_hash);
            $reset_obj->token       = $generated_token; 
            $dt = Carbon::today();
            $validity_date = $dt->addHours(RESET_PASSWORD_LINK_VALIDITY_HOURS);
            $reset_obj->valid_till  =  $validity_date;

            $save_object = $reset_obj->save();            

            if($save_object == true)
            {

                dispatch(new SendEmail($reset_obj,"password-reset-link-mail"))->onqueue("high");
                //$mail_obj = new OutgoingMailsController();
                //$mail_obj->send_password_reset_link_mail($reset_obj,$generated_token);

                
                return true;

            }
            else
            {
                return false;

            }    
            
    
       
       
    }





    public function create_jobseeker_reset_request_api()
    {

        //return "gelloo";
        
        $rules = array(
                        'email'                => ['required','email',new ValidUseremail],    
                        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
                        'email'                => 'Email',    
                        );
        
        $validator = Validator::make(request()->all(), $rules);
        $validator->setAttributeNames($friendly_names);

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            //return response()->json($validator->messages(), 500);//use this to insert status code
            
            $error_check_obj = new ApiErrorController();
            $responses = $error_check_obj->respondValidationError($validator->messages());// this is for single object when you user get() in query
            return $responses;
            //return response()->json($validator->messages(),422);
        }
        else 
        {
            //request()->all();
            $user = User::getUserObject(request('email'),JOBSEEKERCATID);
            
            if($user)
            {
                    $reset_obj = new Password_reset();

                    $reset_obj->userid      = $user->id;
                    $reset_obj->email       = request('email');
                    $key_to_hash            = request('email').time();
                    $generated_token        = base64_encode($key_to_hash);
                    $reset_obj->token       = $generated_token; 
                    $dt = Carbon::today();
                    $validity_date = $dt->addHours(RESET_PASSWORD_LINK_VALIDITY_HOURS);
                    $reset_obj->valid_till  =  $validity_date;

                    $save_object = $reset_obj->save();            

                    if($save_object == true)
                    {

                        $mail_obj = new OutgoingMailsController();
                        $mail_obj->send_password_reset_link_mail($reset_obj);
                        $arrayName = array('status' => 'Password Reset Mail Sent',
                                                        'message' => 'Password Reset Link Sent To Your Mail Successfuly');
                        
                        //$array_obj = (object)$arrayName;

                        //$error_check_obj = new ApiErrorController();            
                        //$responses = $error_check_obj->respondWithOk($array_obj);
                       
                        $return_array = [
                                    'data' =>$arrayName
                                    
                                ];

                        return response()->json($return_array);


                        //return $responses;
                        /*
                        $mail_obj = new OutgoingMailsController();
                        $mail_obj->send_password_reset_link_mail($reset_obj,$generated_token);

                        $title   = "Forgot Password";
                        $message = "Password Reset Link Sent To Your Email Id.";

                        return view("theme1.notifypages.success",compact(['title','message']));
                        */
                    }    
            
            }
            else
            {

                $error_check_obj = new ApiErrorController();            
                $responses = $error_check_obj->respondNotFound();
                return $responses;

            }
        }     
       
    }



    public function create_employer_reset_request_api()
    {

        //return "gelloo";
        
        $rules = array(
                        'email'                => ['required','email',new ValidUseremail],    
                        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
                        'email'                => 'Email',    
                        );
        
        $validator = Validator::make(request()->all(), $rules);
        $validator->setAttributeNames($friendly_names);

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            //return response()->json($validator->messages(), 500);//use this to insert status code
            return response()->json($validator->messages(),422);
        }
        else 
        {
            //request()->all();
            $user = User::getUserEmployerObject(request('email'),EMPLOYERCATID);
            if($user)
            {    
                    $reset_obj = new Password_reset();
                    $reset_obj->userid      = $user->id;
                    $reset_obj->email       = request('email');
                    $key_to_hash            = request('email').time();
                    $generated_token        = base64_encode($key_to_hash);
                    $reset_obj->token       = $generated_token; 
                    $dt = Carbon::today();
                    $validity_date = $dt->addHours(RESET_PASSWORD_LINK_VALIDITY_HOURS);
                    $reset_obj->valid_till  =  $validity_date;

                    $save_object = $reset_obj->save();            

                    if($save_object == true)
                    {

                        $mail_obj = new OutgoingMailsController();
                        $mail_obj->send_password_reset_link_mail($reset_obj);
                        $arrayName = array('status' => 'Password Reset Mail Sent',
                                                        'message' => 'Password Reset Link Sent To Your Mail Successfuly');
                        
                        //$array_obj = (object)$arrayName;

                        //$error_check_obj = new ApiErrorController();            
                        //$responses = $error_check_obj->respondWithOk($array_obj);
                       
                        $return_array = [
                                    'data' =>$arrayName
                                    
                                ];

                        return response()->json($return_array);


                        //return $responses;
                        /*
                        $mail_obj = new OutgoingMailsController();
                        $mail_obj->send_password_reset_link_mail($reset_obj->email,$generated_token);

                        $title   = "Forgot Password";
                        $message = "Password Reset Link Sent To Your Email Id.";

                        return view("theme1.notifypages.success",compact(['title','message']));
                        */
                    }
            }
            else
            {
                $error_check_obj = new ApiErrorController();            
                $responses = $error_check_obj->respondNotFound();
                return $responses;   

            }          
            

        } 
       
    }




    public function display_reset_password_form($token)
    {
        //return $token;

        if($token != null)
        {    
            $reset_obj = Password_reset::getUserObject($token);
            //return $reset_obj;
            //check validity
            if($reset_obj != null)
            {            
                    if($reset_obj->valid_till >= date("Y-m-d") && $reset_obj->is_valid == true)
                    {

                        $user = User::select('categoryid')->where('id','=',$reset_obj->userid)->first();

                        switch($user->categoryid)
                        {

                            case JOBSEEKERCATID:
                                $user = User::getUserObject('','',$reset_obj->userid);
                                $email = $user->email;
                                break;
                            case CONSULTANTCATID:
                                break;
                            case ADMINCATID:
                                break; 
                            case EMPLOYERCATID:
                                $user = User::getUserEmployerObject('','',$reset_obj->userid);
                                $email = $user->email;
                                break;    
                        }
                        return view("theme1.web.reset_password",compact(['email','token']));

                    }
                    else
                    { 
                        $title   = "Link Expired";
                        $message = "Password Reset Link Expired Please Try Again.";   
                        return view("theme1.notifypages.error",compact(['title','message']));
                    }
            }
            else
            {
                $title   = "Invalid Link";
                $message = "This Link Is Not Valid Please Try Again with New Request.";   
                return view("theme1.notifypages.error",compact(['title','message']));

            }   

        }
        else
        {
            $title   = "Unauthorised Link";
            $message = "This Link Is Not Authorised Please Try Again with New Request.";   
            return view("theme1.notifypages.error",compact(['title','message']));


        }    


        

    }


    public function api_change_password()
    {
        //return request()->all();

        $rules = array(
            'txtold_password'                      => ['required', new CheckValidPassword ],    
            'password'                             => 'required|confirmed',
            'password_confirmation'                => 'required',
            
            
        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
            'txtold_password'                      => 'Current Password',    
            'password'                             => 'Password',
            'password_confirmation'                => 'Confirmation',
            
        );
        
        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        $error = new ApiErrorController();

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            //return response()->json($validator->messages(), 500);//use this to insert status code
            //return $validator->validate();
            
            return $error->respondValidationError($validator->messages());
            //return response()->json($validator->messages(),422);
        }
        else 
        {
            DB::beginTransaction();

                

                try {
                        $user_obj = User::where('id','=',request('api_userid'))->first();
                        $user_obj->password       =  Hash::make(request('password'));
                        $user_obj->api_token       = time().str_random(60);
                        $user_obj->update();


                        switch($user_obj->categoryid)
                        {
                            case JOBSEEKERCATID:
                                $message = "Jobseeker Password Updated Successfuly";
                                $mail_obj = new OutgoingMailsController();
                                $mail_obj->send_password_update_success_mail($user_obj);
                                $user = User::getUserObject('','',request('api_userid'));
                                $responses = $error->respondWithOk(new UserResource($user));// this is for single object when you
                                break;
                            case EMPLOYERCATID:
                                $message = "Employer Password Updated Successfuly";
                                $mail_obj = new OutgoingMailsController();
                                $mail_obj->send_password_update_success_mail($user_obj);
                                $user = User::getUserEmployerObject('','',request('api_userid'));
                                $responses = $error->respondWithOk(new EmployerResource($user));// this is for single object when you
                                break;    
                        }
                        DB::commit();

                        return $responses;

                    } catch (\Illuminate\Database\QueryException $e) 
                    {
                        dd($e);
                        DB::rollBack();
                        
                    }    



                

                
                   
        }
        
      }  







    public function update_password()
    {

        //return request()->all();
        $rules = array(
            'txttoken'                         => 'required',    
            'password'                      => 'required|confirmed',
            
            
        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
            'txttoken'                 => 'Compulsory Field',    
            'password'              => 'Password',
            
        );
        
        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            //return response()->json($validator->messages(), 500);//use this to insert status code
            return $validator->validate();
            //return response()->json($validator->messages(),422);
        }
        else 
        {
           //return request()->all();

            $reset_obj = Password_reset::getUserObject(request('txttoken'));
            if($reset_obj != null)
            {
                $user_obj = User::where('id','=',$reset_obj->userid)->first();
                $user_obj->password       =  Hash::make(request('password'));
                $user_obj->api_token       = time().str_random(60);
                $user_obj->update();

                $reset_obj->is_valid = false;
                $reset_obj->update();

                $title = "Password Reset";

                switch($user_obj->categoryid)
                {
                    case JOBSEEKERCATID:
                        $message = "Jobseeker Password Updated Successfuly";
                        
                        dispatch(new SendEmail($user_obj,"password-reset-success-mail"))->onqueue("high");
                        //$mail_obj = new OutgoingMailsController();
                        //$mail_obj->send_password_update_success_mail($reset_obj);
                        break;
                    case CONSULTANTCATID:
                        $message = "Consultant Password Updated Successfuly";
                        $mail_obj = new OutgoingMailsController();
                        $mail_obj->send_password_update_success_mail($reset_obj);
                        break;
                    case ADMINCATID:
                        $message = "Admin Password Updated Successfuly";
                        $mail_obj = new OutgoingMailsController();
                        $mail_obj->send_password_update_success_mail($reset_obj);
                        break;
                    case EMPLOYERCATID:
                        $message = "Employer Password Updated Successfuly";
                        dispatch(new SendEmail($user_obj,"password-reset-success-mail"))->onqueue("high");
                        //$mail_obj = new OutgoingMailsController();
                        //$mail_obj->send_password_update_success_mail($reset_obj);
                        break;    
                }    

                
                return view("theme1.notifypages.success",compact(['title','message']));
            }    

            //return $user;

           // return "password changed";
        }    



    }



    

}
