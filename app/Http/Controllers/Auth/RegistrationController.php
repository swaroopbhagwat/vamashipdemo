<?php

namespace App\Http\Controllers\Auth;

//se PathHelper;

use App\Http\Controllers\Controller;

use App\Http\Controllers\ApiErrorController;

use App\Http\Controllers\OutgoingMailsController;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use App\User;

use App\Mstjobseeker;

use App\Mstemployer;

use App\Mstconsultant;

use App\Trnuser_device_token;

use App\Mstcompany;

use App\LanguageTranslation; 

use Validator;

use Illuminate\Auth\Events\Registered;

use App\Jobs\SendEmail;

use App\Rules\ValidJobseekeremail;

use App\Rules\ValidEmployeremail;

use App\Rules\ValidMobile;

use App\Rules\ValidString;

use App\Rules\ValidUsername;

use App\Http\Resources\UserResource;

use App\Http\Resources\EmployerResource;

use Illuminate\Support\Facades\DB;

// below resource is only to send api errors
use App\Http\Resources\Api_errorResourse;

class RegistrationController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*
    public function __construct()
    {

        $this->middleware('auth');

    }
    */


    public function create_admin()
    {
        //

        //$message = '';

        //return view('theme1.adminregister',compact('message'));


        $pass_val = Get_Root_Path();

        return view('theme1.adminregister',compact('pass_val'));
    }


    public function create_consultant()
    {
        //

        //$message = '';

        //return view('theme1.adminregister',compact('message'));


        $pass_val = Get_Root_Path();

        return view('theme1.consultantregister',compact('pass_val'));
    }

    public function create_assistant_admin()
    {
        //

        //$message = '';

        //return view('theme1.adminregister',compact('message'));


        $pass_val = Get_Root_Path();

        return view('theme1.assistant_admin_register',compact('pass_val'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function adminregister(Request $request)
    {
        //
        //return(request()->all());

        //validate form
        /*  old way  
        $this->validate(request(),[
            'txtusername' => 'required',
            'txtuseremail' => 'required|email',
            'txtusermobileno' => 'required',
            'password' => 'required|confirmed',
            'txtcategoryid' => 'required'

            
        ]);
        */

        // new way

        
        $request->validate([
            'txtusername'       => ['required',new ValidString('name')],
            'txtuseremail'      => 'required|email',
            'txtusermobileno'   => ['required','numeric', new ValidMobile],
            'password'          => 'required',
            'txtcategoryid'     => 'required'

            
        ]);

        //insert record

        $userobj = new User();
        
        $userobj->name = request('txtusername');
        $userobj->email = request('txtuseremail');
        $userobj->usermobileno = request('txtusermobileno');    
        $userobj->password = Hash::make(request('password'));
        //$uniqueId= time().'-'.mt_rand();
        //    

        $userobj->api_token = time().str_random(60);
        $userobj->categoryid = request('txtcategoryid');
        $userobj->email_token = base64_encode(request('txtuseremail'));
        

          

        try {
                $userobj->save();
                
                //dispatch(new SendEmail($userobj));
                dispatch(new SendEmail($userobj,"email-verification-mail"))->onqueue("high");

                $email = $userobj->email;

                return view('theme1.adminpages.verification',compact('email'));

            } catch (\Illuminate\Database\QueryException $e) 
            {
                //dd($e);

                

                //return view('theme1.adminregister',compact('message'));

                return redirect()->back()->withErrors(['message' => 'This Email For Same Category Already Exists.Please Create New One']);
            }

        //sign user in

        //auth()->login($userobj);

        //redirect to home page

        //$data = $request->session()->all();
        //return $data;

        //return redirect()->home();
       //dd(session()->all());



        //return "storing admin user";
    }



    public function consultantregister(Request $request)
    {
        //
        //return(request()->all());

        //validate form
        /*  old way  
        $this->validate(request(),[
            'txtusername' => 'required',
            'txtuseremail' => 'required|email',
            'txtusermobileno' => 'required',
            'password' => 'required|confirmed',
            'txtcategoryid' => 'required'

            
        ]);
        */

        // new way

        
        $request->validate([
            'txtusername'       => ['required',new ValidString('name')],
            'txtuseremail'      => 'required|email',
            'txtusermobileno'   => ['required','numeric', new ValidMobile],
            'password'          => 'required',
            'txtcategoryid'     => 'required'

            
        ]);

        //insert record

        $userobj = new User();
        
        $userobj->name = request('txtusername');

        //$name_in_arabic = LanguageTranslation::get_translation($userobj->name,false);


        //$userobj->name_in_arabic = ($name_in_arabic) ? $name_in_arabic:NULL;


        $userobj->email = request('txtuseremail');
        $userobj->usermobileno = request('txtusermobileno');    
        $userobj->password = Hash::make(request('password'));
        //$uniqueId= time().'-'.mt_rand();
        //    

        $userobj->api_token = time().str_random(60);
        $userobj->categoryid = request('txtcategoryid');
        $userobj->email_token = base64_encode(request('txtuseremail'));
        $userobj->created_by = auth()->user()->id;

          

        try {
                $userobj->save();


                //dispatch(new SendEmail($userobj));
                dispatch(new SendEmail($userobj,"email-verification-mail"))->onqueue("high");

                $email = $userobj->email;

                return view('theme1.adminpages.verification',compact('email'));

            } catch (\Illuminate\Database\QueryException $e) 
            {
                //dd($e);

                

                //return view('theme1.adminregister',compact('message'));

                return redirect()->back()->withErrors(['message' => 'This Email For Same Category Already Exists.Please Create New One']);
            }

        //sign user in

        //auth()->login($userobj);

        //redirect to home page

        //$data = $request->session()->all();
        //return $data;

        //return redirect()->home();
       //dd(session()->all());



        //return "storing admin user";
    }

    public function assistant_admin_register(Request $request)
    {
        
        
        $request->validate([
            'txtusername'       => ['required',new ValidString('name')],
            'txtuseremail'      => 'required|email',
            'txtusermobileno'   => ['required','numeric', new ValidMobile],
            'password'          => 'required',
            'txtcategoryid'     => 'required'

            
        ]);

        //insert record

        $userobj = new User();
        
        $userobj->name = request('txtusername');

        //$name_in_arabic = LanguageTranslation::get_translation($userobj->name,false);

        //$userobj->name_in_arabic = ($name_in_arabic) ? $name_in_arabic:NULL;


        $userobj->email = request('txtuseremail');
        $userobj->usermobileno = request('txtusermobileno');    
        $userobj->password = Hash::make(request('password'));
        //$uniqueId= time().'-'.mt_rand();
        //    

        $userobj->api_token = time().str_random(60);
        $userobj->categoryid = request('txtcategoryid');
        $userobj->email_token = base64_encode(request('txtuseremail'));
        $userobj->created_by = auth()->user()->id;

          

        try {
                $userobj->save();


                //dispatch(new SendEmail($userobj));
                dispatch(new SendEmail($userobj,"email-verification-mail"))->onqueue("high");

                $email = $userobj->email;

                return view('theme1.adminpages.verification',compact('email'));

            } catch (\Illuminate\Database\QueryException $e) 
            {
                //dd($e);

                

                //return view('theme1.adminregister',compact('message'));

                return redirect()->back()->withErrors(['message' => 'This Email For Same Category Already Exists.Please Create New One']);
            }

    


    }


    






    public function apijobseekerregister(Request $request)
    {
        

        $error_check_obj = new ApiErrorController();
        //return print_r ($error_check_obj);

        $rules = array(
            'txtusername'       => ['required',new ValidString('name')],
            'txtuseremail'      => ['required','email',new ValidJobseekeremail],
            'txtcountrycode'    => 'required|numeric',    
            'txtusermobileno'   => ['required','numeric', new ValidMobile],
            'password'          => 'required',
            'txtcategoryid'     => 'required',
            'txtcountryid'      => 'required',
            'txtgcm_token'      => 'required',
            'txtdevice_type_id'      => 'required',
            'txtterms_accepted'    => 'required',
            'txtis_mobile_verified'     => 'required|in:1',
            
            
        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
            'txtusername'       => 'Name',
            'txtuseremail'      => 'Email',
            'txtcountrycode'    => 'Contry Code',    
            'txtusermobileno'   => 'Mobile No',
            'password'          => 'Password',
            'txtcategoryid'     => 'Compulsory Field',
            'txtcountryid'      => 'Country',
            'txtgcm_token'      => 'GCM Token',
            'txtdevice_type_id'      => 'Device Type',
            'txtterms_accepted'    => 'Terms',
            'txtis_mobile_verified'     => 'Mobile Verification',
        );
        
        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            
            $responses = $error_check_obj->respondValidationError($validator->messages());

        }
        else 
        {
            //insert record
            DB::beginTransaction();

                $userobj = new User();
                
                $userobj->name = request('txtusername');

                //$name_in_arabic = LanguageTranslation::get_translation($userobj->name,false);

                //$userobj->name_in_arabic = ($name_in_arabic) ? $name_in_arabic:NULL;


                $userobj->email = request('txtuseremail');
                $userobj->countrycode = request('txtcountrycode');    
                $userobj->usermobileno = request('txtusermobileno');    
                $userobj->password = Hash::make(request('password'));
                $userobj->mobile_verified = request('txtis_mobile_verified');
                $userobj->activation_status = 2;

                //$uniqueId= time().'-'.mt_rand();
                //    

                $userobj->api_token = time().str_random(60);
                $userobj->categoryid = request('txtcategoryid');//request('txtcategoryid');
                $userobj->email_token = base64_encode(request('txtuseremail'));
                
                    

                try {
                        $userobj->save();

                        //save GCM Token

                        $token = new Trnuser_device_token();
                        $token->userid          = $userobj->id;
                        $token->device_type     = request('txtdevice_type_id');
                        $token->gcm_token    = request('txtgcm_token');
                        $token->device_model_name       = request('txtdevice_model_name');
                        $token->device_os_version       = request('txtdevice_os_version');   
                        $token->is_latest       = true;
                        $token->created_by       = $userobj->id;
                        Trnuser_device_token::update_latest_status($userobj->id,$userobj->id,request('txtdevice_type_id'));
                        $token_obj = $token->save();

                        
                        
                        $jobseeker = new Mstjobseeker();
                        //return print_r ($jobseeker);
                        
                        $jobseeker->userid    = $userobj->id;
                        $jobseeker->countryid = request('txtcountryid');
                        $jobseeker->terms_accepted = request('txtterms_accepted');
                        $jobseeker->created_by = $userobj->id;
                        $jobseeker->save();
                        //dd($jobseeker);
                        
                        $user = User::getUserObject('','',$userobj->id); // this function you will find in user model
                        
                        try {

                                dispatch(new SendEmail($user,"jobseeker-welcome-mail"))->onqueue("high");
                                dispatch(new SendEmail($user,"email-verification-mail"))->onqueue("high");
   

                            } catch (\Illuminate\Database\QueryException $e) 
                            {
                                //dd($e);

                                
                                $responses =  $error_check_obj->respondInternalServerError();
                                //return view('theme1.adminregister',compact('message'));

                                //return redirect()->back()->withErrors(['message' => 'This Email For Same Category Already Exists.Please Create New One']);
                            }


                        

                        DB::commit();
                       
                        $responses = $error_check_obj->respondWithOk( new UserResource($user));// this is for single object when you user first() in query
                                //return new UserResource($user);
                          
                    } catch (\Illuminate\Database\QueryException $e) 
                    {
                        DB::rollBack();
                        //dd($e);

                        
                        $responses = $error_check_obj ->respondDuplicateEntry();

                    }

        }

         return $responses;       
        
    }




    public function apiemployerregister(Request $request)
    {
        
        //return request()->all();

        $error_check_obj = new ApiErrorController();
        //return print_r ($error_check_obj);

        $rules = array(
            'txtusername'           => ['required',new ValidString('name')],
            //'txtuseremail'          => 'required',
            'txtuseremail'          => ['required','email',new ValidEmployeremail],
            'txtcountrycode'        => 'required|numeric',    
            'txtusermobileno'       => ['required','numeric', new ValidMobile],
            'password'              => 'required',
            'txtcategoryid'         => 'required',
            'txtcountryid'          => 'required',
            //'txtaddressline1'       => ['required',new ValidString('address')],
            //'txtstateid'            => 'required',
            //'txtcityid'             => 'required',
            //'txtpincode'            => 'required|numeric',
            'txtgcm_token'          => 'required',
            'txtdevice_type_id'     => 'required',
            'txtterms_accepted'     => 'required',
            'txtis_mobile_verified'     => 'required|in:1',
            
            
        );

        // Another way to change attribute name is set attribute names in validate.php file

        $friendly_names = array(
            'txtusername'       => 'Name',
            'txtuseremail'      => 'Email',
            'txtcountrycode'    => 'Contry Code',    
            'txtusermobileno'   => 'Mobile No',
            'password'          => 'Password',
            'txtcategoryid'     => 'Compulsory Field',
            'txtcountryid'      => 'Country',
            //'txtaddressline1'       => 'Address',
            //'txtstateid'            => 'State',
            //'txtcityid'             => 'City',
            //'txtpincode'            => 'Zipcode',
            'txtgcm_token'          => 'Notification Flag',
            'txtdevice_type_id'     => 'Device Type',
            'txtterms_accepted'    => 'Terms',
            'txtis_mobile_verified'     => 'Mobile Verification',
        );
        
        $messages = [
            'txtis_mobile_verified.required' => 'Please verify your mobile number first.',
        ];

        $validator = Validator::make(request()->all(),$rules,$messages);
        $validator->setAttributeNames($friendly_names);

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            
            $responses = $error_check_obj->respondValidationError($validator->messages());

        }
        else 
        {
            //insert record

                DB::beginTransaction();

                $userobj = new User();
                
                $userobj->name = LtoRtrim(request('txtusername'));
                //$name_in_arabic = LanguageTranslation::get_translation($userobj->name,false);

                //$userobj->name_in_arabic = ($name_in_arabic) ? $name_in_arabic:NULL;


                $userobj->email = request('txtuseremail');
                $userobj->countrycode = request('txtcountrycode');    
                $userobj->usermobileno = request('txtusermobileno');
                $userobj->mobile_verified = request('txtis_mobile_verified');    
                $userobj->password = Hash::make(request('password'));

                //$uniqueId= time().'-'.mt_rand();
                //    

                $userobj->api_token = time().str_random(60);
                $userobj->categoryid = EMPLOYERCATID;//request('txtcategoryid');//request('txtcategoryid');
                $userobj->email_token = base64_encode(request('txtuseremail'));
                
                    

                try {
                        $userobj->save();
                        
                        //save GCM Token

                        $token = new Trnuser_device_token();
                        $token->userid          = $userobj->id;
                        $token->device_type     = request('txtdevice_type_id');
                        $token->gcm_token    = request('txtgcm_token');   
                        $token->device_model_name       = request('txtdevice_model_name');
                        $token->device_os_version       = request('txtdevice_os_version');
                        $token->is_latest       = true;
                        $token->created_by       = $userobj->id;
                        Trnuser_device_token::update_latest_status($userobj->id,$userobj->id,request('txtdevice_type_id'));
                        $token_obj = $token->save();
                        

                        //save employer record
                        $employer = new Mstemployer();
                        //return print_r ($jobseeker);
                        
                        $employer->userid    = $userobj->id;
                        $employer->countryid = request('txtcountryid');
                        $employer->addressline1 = request('txtaddressline1');
                        $employer->stateid = request('txtstateid');
                        $employer->cityid = request('txtcityid');
                        $employer->pincode = request('txtpincode');
                        $employer->terms_accepted = request('txtterms_accepted');
                        $employer->created_by = $userobj->id;
                        $employer_obj = $employer->save();
                        
                        if(request('txtcompany_name') > '')
                        {

                            //save Company record
                            $company = new Mstcompany();
                            //return print_r ($jobseeker);
                            
                            $company->userid    = $userobj->id;
                            $company->company_name = request('txtcompany_name');
                            $company->company_email = request('txtuseremail');
                            $company->mobileno = request('txtusermobileno');
                            $company->addressline1 = request('txtaddressline1');
                            $company->countryid = request('txtcountryid');
                            $company->countrycode = request('txtcountrycode');
                            $company->stateid = request('txtstateid');
                            $company->cityid = request('txtcityid');
                            $company->pincode = request('txtpincode');
                            $company->created_by = $userobj->id;
                            $company_obj = $company->save();

                        }    


                        $user = User::getUserEmployerObject('','',$userobj->id); // this function you will find in user model
                        
                        DB::commit();

                        $responses = $error_check_obj->respondWithOk( new EmployerResource($user));// this is for single object when you user first() in query
                         
                        try {
                                /*
                                dispatch(new SendEmail($user,"employer-welcome-mail"));
                                dispatch(new SendEmail($user,"email-verification-mail"));
                                */  

                            } catch (\Illuminate\Database\QueryException $e) 
                            {
                                //dd($e);

                                $responses =  $error_check_obj->respondInternalServerError();

                                //return view('theme1.adminregister',compact('message'));

                                //return redirect()->back()->withErrors(['message' => 'This Email For Same Category Already Exists.Please Create New One']);
                            }


                        


                    } catch (\Illuminate\Database\QueryException $e) 
                    {
                        dd($e);
                         // Add the exception class name, message and stack trace to response
                                    $responses['exception'] = get_class($e); // Reflection might be better here
                                    $responses['message'] = $e->getMessage();
                                    $responses['trace'] = $e->getTrace();
                                
                                // Default response of 400
                                //$status = 400;

                                // If this exception is an instance of HttpException
                                

                                // Return a JSON response with the response array and status code
                                //return response()->json($response);
  
                                    DB::rollBack();

                            $responses = $error_check_obj ->respondDuplicateEntry();        

                        //$error_obj = new ApiErrorController();

                        //$responses = $error_obj ->respondDuplicateEntry();                       
                        //return response()->json($arrayName,406);

                        
                    }

        }

         return $responses;       
        
    }





    /*
    public function apiuserregister(Request $request)
    {
        
        // new way
        $request->validate([
            'txtusername' => 'required',
            'txtuseremail' => 'required|email',
            'txtusermobileno' => ['required', new ValidMobile],
            'password' => 'required|confirmed',
            'txtcategoryid' => 'required'

            
        ]);

        //insert record

        $userobj = new User();
        
        $userobj->name = request('txtusername');
        $userobj->email = request('txtuseremail');
        $userobj->usermobileno = request('txtusermobileno');    
        $userobj->password = Hash::make(request('password'));
        //$uniqueId= time().'-'.mt_rand();
        //    

        $userobj->api_token = time().str_random(60);
        $userobj->categoryid = request('txtcategoryid');
        $userobj->email_token = base64_encode(request('txtuseremail'));
        
            

        try {
                $userobj->save();
                
                $user = $userobj
                    ->join('mstusercategories','users.categoryid','=','mstusercategories.id')
                    ->select('users.*', 'mstusercategories.category_name as categoryname')
                    ->first();

                return new UserResource($user);
                    
               
            } catch (\Illuminate\Database\QueryException $e) 
            {
                //dd($e);

                $arrayName = array('status' => 'failed Registration',
                                    'message' => 'This Email For Same Category Already Exists.Please Create New One');    
                return response()->json($arrayName,500);

                //return view('theme1.adminregister',compact('message'));

                return redirect()->back()->withErrors([]);
            }

        
    }
    */



    
    
    /**

    ********************* This Is Specificaly for Email Verification ***************

    * Handle a registration request for the application.

    *

    * @param \Illuminate\Http\Request $request

    * @return \Illuminate\Http\Response

    */
    /*

    public function register(Request $request)

    {

    $this->validator($request->all())->validate();

    event(new Registered($user = $this->create($request->all())));

    $email = request('txtuseremail');

    dispatch(new SendEmail($user));

    return view('verification');

    }
    */

    /**

    * Handle a registration request for the application.

    *

    * @param $token

    * @return \Illuminate\Http\Response

    */

    public function verify($token)

    {

      // return view('emailconfirm'); 
    
        $user = User::where('email_token',$token)->first();

        $user->verified = 1;

        if($user->save())
        {

            return view('theme1.web.email_confirmation_success',['user'=>$user]);
   
        }
     

    }

    /*
    ********************* This Is Specificaly for Email Verification ***************
    */

}
