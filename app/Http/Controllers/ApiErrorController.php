<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\Response as IlluminateResponse;

// below resource is only to send api errors
use App\Http\Resources\Api_errorResourse;

class ApiErrorController extends Controller
{


	public function __construct()
    {

        $this->middleware('auth');

    }
	
    //
	protected $statusCode = 200;
	
	//****************************************************** REUSABLE METHODS *****************************************************
	public function getResponseStatusCode()
	{
		return $this->statusCode;
		
	}
	
	public function setResponseStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;
		return $this; //for chainnig
	}


	public function respondWithError($status,$message)
	{

		$array_response = array('status' => $status,'message' => $message);    
        

        $array_obj = (object)$array_response;
        return (new Api_errorResourse($array_obj))->response()->setStatusCode($this->getResponseStatusCode());
		/*
		$return_array = [
							'error' =>
							[
								'message' => $message,
								'status_code' => $this->getResponseStatusCode(),
							]
						];
		*/				

		//return $this->respond($return_array);
		
	}



	public function respondSuccessWithConstantArray($data)
	{

		//$array_response = array('status' => $status,'message' => $message);    
        
        $responce = array('data' => $data);
        
        return response()->json($responce,IlluminateResponse::HTTP_OK);
        //return $responce;
		//return $this->respond($return_array);
		
	}

	
	
	public function respondWithOk($data)
	{
		return $data->response()->setStatusCode(IlluminateResponse::HTTP_OK);
		
	}
	

	
	public function respondNotFound($status = "Record Not Found",$message = "Not Found Exception")
	{
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($status,$message);
		
	}

	// NOTE : If you use no content it will return nothing
	public function respondNoContent($status="Record Not Found",$message ="There Is No Content")
	{
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_NO_CONTENT)->respondWithError($status,$message);
		
	}

	public function respondUnauthorized($status="User Not Present",$message ="Unauthorized User")
	{
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_UNAUTHORIZED)->respondWithError($status,$message);
		
	}

	
	public function respondValidationError($errors='')
	{
		
		return response()->json($errors,IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
		
	}

	public function respondUnauthorizedRequest($status="This Request Is Not Acceptable",$message ="Unauthorized Request")
	{
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_NOT_ACCEPTABLE)->respondWithError($status,$message);
		
	}

	public function respondUnauthorizedUsertoken($status="This User Is Not Authorised",$message ="Unauthorized User")
	{
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_UNAUTHORIZED)->respondWithError($status,$message);
		
	}

	
	public function respondInternalServerError($status="Internal Server Error",$message ="Something Went Wrong.")
	{
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($status,$message);
		
	}
	
	
	public function respondCreatedSuccessfully($data)
	{
		
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_CREATED)->respondWithOk($data);
		
	}

	public function respondMailSentSuccessfully($status="Mail Sent",$message ="Mail Sent Successfully")
	{
		
		
		return $this->setResponseStatusCode(IlluminateResponse::HTTP_CREATED)->respondWithError($status,$message);
		
	}


	
}
