<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\ApiErrorController;

use App\Http\Resources\AddressbookResource;

use App\Addressbook;

use App\City;

use App\State;

use App\Country;

Use View;

Use Validator;

class AddressbookController extends Controller
{
    //





	public function index()
    {
        $addresses = Addressbook::select('*','a.id as address_id')
        			->from('addressbooks as a')
        			->join('states as b','a.stateid','=','b.id')
        			->join('countries as c','a.countryid','=','c.id')
        			->where('a.userid','=',auth()->user()->id)->get();
        
        return view('theme.pages.user_home',compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     	
     	
        $default_array['address_id']         = (old('txtaddress_id')) ? old('txtaddress_id'):'';
        $default_array['address_book_title']      = (old('txtaddress_book_title')) ? old('txtaddress_book_title'):'';
        $default_array['contact_person_name']      = (old('txtcontact_person_name')) ? old('txtcontact_person_name'):'';
        $default_array['contact_person_number']      = (old('txtcontact_person_number')) ? old('txtcontact_person_number'):'';
        $default_array['addressline1']                = (old('txtaddressline1')) ? old('txtaddressline1'):'';
        $default_array['addressline2']                = (old('txtaddressline2')) ? old('txtaddressline2'):'';
        $default_array['addressline3']                = (old('txtaddressline3')) ? old('txtaddressline3'):'';

        $default_array['pincode']      			= (old('txtpincode')) ? old('txtpincode'):'';
        $default_array['cityid']      			= (old('txtcityid')) ? old('txtcityid'):'';
        $cityid = $default_array['cityid'];
        $default_array['stateid']      			= (old('txtstateid')) ? old('txtstateid'):'';
        $stateid = $default_array['stateid'];
        $default_array['countryid']      			= (old('txtcountryid')) ? old('txtcountryid'):'';
        $countryid = $default_array['countryid'];    

        $citylist = City::city_list();
       	$view = View::make('theme.renderpages.citycombo', compact('citylist','cityid'));
        $city_combo = (string) $view;

        $statelist = State::state_list();
       	$view = View::make('theme.renderpages.statecombo', compact('statelist','stateid'));
        $state_combo = (string) $view;

        $countrylist = Country::country_list();
       	$view = View::make('theme.renderpages.countrycombo', compact('countrylist','countryid'));
        $country_combo = (string) $view;
		
        return view('theme.pages.address_create',compact('default_array','city_combo','state_combo','country_combo'));
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //return request()->all();
        
        $rules = array(
            'txtaddress_book_title'              => 'required',
            'txtcontact_person_name'     	=> 'required',
            'txtcontact_person_number'      => 'required',
            'txtaddressline1'         		=> 'required',
            'txtaddressline2'         		=> 'required',
            'txtpincode'           			=> 'required|numeric',
            'txtcityid'           			=> 'required',
            'txtstateid'           			=> 'required',
            'txtcountryid'           		=> 'required',
            
               
        );

        
        $friendly_names = array(
            'txtaddress_book_title'              => 'Title',
            'txtcontact_person_name'     	=> 'Person Name',
            'txtcontact_person_number'      => 'Contact No.',
            'txtaddressline1'         		=> 'Address Line 1',
            'txtaddressline2'         		=> 'Address Line 2',
            'txtpincode'           			=> 'Pincode',
            'txtcityid'           			=> 'City',
            'txtstateid'           			=> 'State',
            'txtcountryid'           		=> 'Country',
            
            
        );
        
        

        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        


        if ($validator->fails()) {    
            
            return back()->withInput()->withErrors($validator);
        
        }
        else
        {

                $addressobj = new Addressbook();
                
                $addressobj->userid = auth()->user()->id;
                $addressobj->address_book_title = ucwords(request('txtaddress_book_title'));
                $addressobj->contact_person_name = ucwords(request('txtcontact_person_name'));
                $addressobj->contact_person_number= request('txtcontact_person_number');
                $addressobj->addressline1 = request('txtaddressline1');
                $addressobj->addressline2 = request('txtaddressline2');
                $addressobj->addressline3 = request('txtaddressline3');
                $addressobj->pincode = request('txtpincode');    
                $addressobj->cityid = request('txtcityid');
                $addressobj->stateid = request('txtstateid');
                $addressobj->countryid = request('txtcountryid');
                
                $addressobj->save();

                
                return redirect('/user-home');
      }  
        
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mstcountry  $mstcountry
     * @return \Illuminate\Http\Response
     */
    public function edit(Addressbook $addressbook,$aid)
    {
        //
        $address_id = $aid;
        $default_array = Addressbook::select('*','id as address_id')
        				->where('id','=',$address_id)->first();
        
        $cityid = $default_array['cityid'];
		$stateid = $default_array['stateid'];
		$countryid = $default_array['countryid'];

		$citylist = City::city_list();
       	$view = View::make('theme.renderpages.citycombo', compact('citylist','cityid'));
        $city_combo = (string) $view;

        $statelist = State::state_list();
       	$view = View::make('theme.renderpages.statecombo', compact('statelist','stateid'));
        $state_combo = (string) $view;

        $countrylist = Country::country_list();
       	$view = View::make('theme.renderpages.countrycombo', compact('countrylist','countryid'));
        $country_combo = (string) $view;
       
        return view('theme.pages.address_create',compact('default_array','city_combo','state_combo','country_combo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mstcountry  $mstcountry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Addressbook $addressbook)
    {
        //

        $rules = array(
        	'txtaddress_id'					=>'required',
            'txtaddress_book_title'              => 'required',
            'txtcontact_person_name'     	=> 'required',
            'txtcontact_person_number'      => 'required',
            'txtaddressline1'         		=> 'required',
            'txtaddressline2'         		=> 'required',
            'txtpincode'           			=> 'required|numeric',
            'txtcityid'           			=> 'required',
            'txtstateid'           			=> 'required',
            'txtcountryid'           		=> 'required',
            
               
        );

        
        $friendly_names = array(
        	'txtaddress_id'					=>'Identifier',
            'txtaddress_book_title'              => 'Title',
            'txtcontact_person_name'     	=> 'Person Name',
            'txtcontact_person_number'      => 'Contact No.',
            'txtaddressline1'         		=> 'Address Line 1',
            'txtaddressline2'         		=> 'Address Line 2',
            'txtpincode'           			=> 'Pincode',
            'txtcityid'           			=> 'City',
            'txtstateid'           			=> 'State',
            'txtcountryid'           		=> 'Country',
            
            
        );
        
        

        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        if ($validator->fails()) {    
            
            return back()->withInput()->withErrors($validator);
        
        }
        else
        {

                $addressobj = Addressbook::where('id','=',request('txtaddress_id'))->first();
                
                $addressobj->userid = auth()->user()->id;
                $addressobj->address_book_title = ucwords(request('txtaddress_book_title'));
                $addressobj->contact_person_name = ucwords(request('txtcontact_person_name'));
                $addressobj->contact_person_number= request('txtcontact_person_number');
                $addressobj->addressline1 = request('txtaddressline1');
                $addressobj->addressline2 = request('txtaddressline2');
                $addressobj->addressline3 = request('txtaddressline3');
                $addressobj->pincode = request('txtpincode');    
                $addressobj->cityid = request('txtcityid');
                $addressobj->stateid = request('txtstateid');
                $addressobj->countryid = request('txtcountryid');
                
                $addressobj->update();

                
                return redirect('/user-home');
      }  		


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mstcountry  $mstcountry
     * @return \Illuminate\Http\Response
     */
    public function delete(Addressbook $addressbook,$aid)
    {
        

        $addobj = Addressbook::find($aid);

        $addobj->delete();

        return redirect('/user-home');


    }




    // ************* API Functions **************

    public function api_address_show($addressid)
    {
        $error_check_obj = new ApiErrorController();

        $address = Addressbook::select('*')->where('id','=',$addressid)->first();

        if($address)
         {

            //return $address;
            $response = $error_check_obj->respondWithOk(new AddressbookResource($address));
         }
         else
         {

            $response = $error_check_obj->respondNotFound();
         }   

         return $response;

    }


    public function api_address_list()
    {
        $error_check_obj = new ApiErrorController();

        $address = Addressbook::select('*')->where('userid','=',request('api_userid'))->get();

        if($address)
         {

            //return $address;
            $response = $error_check_obj->respondWithOk(AddressbookResource::collection($address));
         }
         else
         {

            $response = $error_check_obj->respondNotFound();
         }   

         return $response;

    }

    public function api_address_create()
    {

         $rules = array(
            'txtaddress_book_title'              => 'required',
            'txtcontact_person_name'        => 'required',
            'txtcontact_person_number'      => 'required',
            'txtaddressline1'               => 'required',
            'txtaddressline2'               => 'required',
            'txtpincode'                    => 'required|numeric',
            'txtcityid'                     => 'required',
            'txtstateid'                    => 'required',
            'txtcountryid'                  => 'required',
            
               
        );

        
        $friendly_names = array(
            'txtaddress_book_title'              => 'Title',
            'txtcontact_person_name'        => 'Person Name',
            'txtcontact_person_number'      => 'Contact No.',
            'txtaddressline1'               => 'Address Line 1',
            'txtaddressline2'               => 'Address Line 2',
            'txtpincode'                    => 'Pincode',
            'txtcityid'                     => 'City',
            'txtstateid'                    => 'State',
            'txtcountryid'                  => 'Country',
            
            
        );
        
        

        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        


        if ($validator->fails()) {    
            
            //return back()->withInput()->withErrors($validator);
            return response()->json($validator->messages(),422);
        }
        else
        {

                $addressobj = new Addressbook();
                
                $addressobj->userid = request('api_userid');
                $addressobj->address_book_title = ucwords(request('txtaddress_book_title'));
                $addressobj->contact_person_name = ucwords(request('txtcontact_person_name'));
                $addressobj->contact_person_number= request('txtcontact_person_number');
                $addressobj->addressline1 = request('txtaddressline1');
                $addressobj->addressline2 = request('txtaddressline2');
                $addressobj->addressline3 = request('txtaddressline3');
                $addressobj->pincode = request('txtpincode');    
                $addressobj->cityid = request('txtcityid');
                $addressobj->stateid = request('txtstateid');
                $addressobj->countryid = request('txtcountryid');
                
                $addressobj->save();

                $error_check_obj = new ApiErrorController();
                return $error_check_obj->respondWithOk(new AddressbookResource($addressobj));
      }


    }



    public function api_address_update()
    {
        //

        $rules = array(
            'txtaddress_id'                 =>'required',
            'txtaddress_book_title'              => 'required',
            'txtcontact_person_name'        => 'required',
            'txtcontact_person_number'      => 'required',
            'txtaddressline1'               => 'required',
            'txtaddressline2'               => 'required',
            'txtpincode'                    => 'required|numeric',
            'txtcityid'                     => 'required',
            'txtstateid'                    => 'required',
            'txtcountryid'                  => 'required',
            
               
        );

        
        $friendly_names = array(
            'txtaddress_id'                 =>'Identifier',
            'txtaddress_book_title'              => 'Title',
            'txtcontact_person_name'        => 'Person Name',
            'txtcontact_person_number'      => 'Contact No.',
            'txtaddressline1'               => 'Address Line 1',
            'txtaddressline2'               => 'Address Line 2',
            'txtpincode'                    => 'Pincode',
            'txtcityid'                     => 'City',
            'txtstateid'                    => 'State',
            'txtcountryid'                  => 'Country',
            
            
        );
        
        

        $validator = Validator::make(request()->all(),$rules);
        $validator->setAttributeNames($friendly_names);

        if ($validator->fails()) {    
            
            return response()->json($validator->messages(),422);
        
        }
        else
        {

                $addressobj = Addressbook::where('id','=',request('txtaddress_id'))->first();
                
                $addressobj->userid = request('api_userid');
                $addressobj->address_book_title = ucwords(request('txtaddress_book_title'));
                $addressobj->contact_person_name = ucwords(request('txtcontact_person_name'));
                $addressobj->contact_person_number= request('txtcontact_person_number');
                $addressobj->addressline1 = request('txtaddressline1');
                $addressobj->addressline2 = request('txtaddressline2');
                $addressobj->addressline3 = request('txtaddressline3');
                $addressobj->pincode = request('txtpincode');    
                $addressobj->cityid = request('txtcityid');
                $addressobj->stateid = request('txtstateid');
                $addressobj->countryid = request('txtcountryid');
                
                $addressobj->update();

                
                $error_check_obj = new ApiErrorController();
                return $error_check_obj->respondWithOk(new AddressbookResource($addressobj));
      }         


    }



    public function api_address_delete($aid)
    {
        

        $addobj = Addressbook::find($aid);

        $addobj->delete();

        //return redirect('/user-home');
        $error_check_obj = new ApiErrorController();

        $message =  array('status' => 'Address Deleted','message' => 'Address Removed Successfully');
        return $error_check_obj->respondSuccessWithConstantArray($message);

    }




}
