<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

use App\Providers\AppServiceProvider;


//use Illuminate\Http\Resources\Json\ResourceCollection;


class Api_errorResourse extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */

    /*
    public function __construct($obj)
    {

        return $this->obj;

    }
    */

    public function toArray($request)
    {
        //return parent::toArray($request);

        Resource::wrap('error');

        return [
            'status' => $this->status,
            'message' => $this->message,  
            
        ];
    }


    /*
    public function with($request)
    {
        //return parent::toArray($request);

        return [
            'version' => "v1",
            

        ];
    }
    */



}
