<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Country;

use App\Http\Resources\CountryResource;

use App\State;

use App\Http\Resources\StateResource;

use App\City;

use App\Http\Resources\CityResource;

class AddressbookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            "addressid" => $this->id,
            "address_book_title" => $this->address_book_title,
            "contact_person_name" => $this->contact_person_name,
            "contact_person_number" => $this->contact_person_number,
            "addressline1" => $this->addressline1,
            "addressline2" => $this->addressline2,
            "addressline3" => $this->addressline3,
            "pincode" => $this->pincode,
            "country" => Country::country($this->countryid),
            "state" => State::state($this->stateid),
            "city" => City::city($this->cityid),
            
        ];
    }
}
