<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use App\Http\Controllers\ApiErrorController;


class ApiTokenAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function checkvalidapitoken()
    {

         $user = user::where('users.id',Auth::guard('api')->id())
                    ->select('users.*')
                    ->first();
         //dd($user);


        if ($user) 
        {
            $userid = $user['id'];
            return $userid;
            # code...
        }
        else
        {
            return false;
        }    


    }

    public function handle($request, Closure $next)
    {
            $error_check_obj = new ApiErrorController();

            //**** note : key name containing "_" will not be send to user so use "-" instead of undescore ******
            if ($request->hasHeader('api-token') === false) 
            {

                //return response()->json('Unauthorised Token Request');
                $responses = $error_check_obj->respondUnauthorizedUsertoken();
                return $responses;

            }
            else
            {

                $header_token = $request->header('api-token');
                //return response()->json($header_token);

                $request->request->add(['api_token' => $header_token]);
                $check_result  = $this->checkvalidapitoken($header_token);
                    
                //return response()->json($check_result);                
                if($check_result === false)
                { 
                    $request->request->remove('api_token');
                    $status = "Api Token Not Matching";
                    $message = "Invalid User";
                    $responses = $error_check_obj->respondUnauthorizedUsertoken($status,$message);
                    return $responses;
                }
                else 
                {
                    $request->request->add(['api_userid' => $check_result]);
                    $request->request->add(['req_type' => "api"]);
                    //return response()->json($request);   
                }
                    
                        
            }    
            
            
        return $next($request);

        
    }

}
