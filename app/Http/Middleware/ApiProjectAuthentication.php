<?php

namespace App\Http\Middleware;

use Closure;

use App\Http\Controllers\ApiErrorController;

class ApiProjectAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            $error_check_obj = new ApiErrorController();

            if ($request->hasHeader('project-token') === false) 
            {
                //return response()->json('Unauthorised Request');
                $responses = $error_check_obj->respondUnauthorizedRequest();
                return $responses;

            }
            else
            {
                
                             if ($request->header('project-token') != 'vamaship@token') 
                                {
                                
                                    //return response()->json('Header Not Matching');
                                    $status = "Device Token Is Not Matching";
                                    $message = "Unathorised Request";
                                    $responses = $error_check_obj->respondUnauthorizedRequest($status,$message);
                                    return $responses;    

                                }
                
            }

            return $next($request);

    }
            
            
        

        

}

