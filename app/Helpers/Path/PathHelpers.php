<?php

    //******************************* Absolute path or url path **********************************************
    function Get_Domain_Url() //responsible for http://domain.com/
    {

        return Config('app.url').'/';
    }
  

    function Get_Root_Url() //responsible for http://domain.com/webapp/
    {

    	return url('').'/';
    }

    

    function Get_Images_Url()//responsible for http://domain.com/webapp/public/images/ 
    {
        return Get_Root_Url().'assets/img/';
    }



    //********************** CSS/JS ****************************

    function Get_Public_Css_Url()//responsible for http://domain.com/public/css/ 
    {

        
        return Get_Root_Url().'assets/css/';
    }

    function Get_Public_Js_Url()//responsible for http://domain.com/public/js/ 
    {

        return Get_Root_Url().'assets/js/';
    }

    

    