<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //

	public static function country($countryid)
    {
    	$country_obj = Country::select('id','country_name')->where('id','=',$countryid)->first();
    
        return $country_obj;

    }


    public static function country_list()
    {

    	    $allcountries = Country::select('id','country_name')
                        ->orderby('id','asc')     
                        ->get();


        return $allcountries;

    }

}
